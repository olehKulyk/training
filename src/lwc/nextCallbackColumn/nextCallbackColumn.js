import {api, LightningElement} from 'lwc';

export default class NextCallbackColumn extends LightningElement {

    textColor = 'slds-has-flexi-truncate ';
    _nextCallbackDate;

    @api
    set nextCallbackDate(value) {
        this._nextCallbackDate = value;
        if (!this._nextCallbackDate) return;
        let callbackDate = new Date(this._nextCallbackDate);
        let today = new Date();
        this.textColor += callbackDate < today ? 'red-text' : 'green-text';
    }

    get nextCallbackDate() {
        return this._nextCallbackDate;
    }
}