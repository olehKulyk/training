import LightningDatatable  from "lightning/datatable";
import nextCallbackColumn from "./nextCallbackColumn.html";

export default class CustomAccountDatatable extends LightningDatatable {
    static customTypes = {
        nextCallbackColumn: {
            template: nextCallbackColumn,
            standardCellLayout: true,
            typeAttributes: ["nextCallbackDate"]
        }
    };
}