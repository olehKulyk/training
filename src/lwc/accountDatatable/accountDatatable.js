import {LightningElement, track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getAccounts from '@salesforce/apex/AccountDatatableController.getAccounts';
import getAccountsSize from '@salesforce/apex/AccountDatatableController.getAccountsSize';
import getAccountFields from '@salesforce/apex/AccountDatatableController.getAccountFields';

const debounceTimeout = 400;

const columns = [
    {
        type: "button", typeAttributes: {
            label: 'Edit',
            name: 'Edit',
            title: 'Edit',
            disabled: false,
            value: 'edit',
            iconPosition: 'left',
            iconName: 'utility:edit'
        },
        initialWidth: 95
    },
    {
        label: 'Name', fieldName: 'recordUrl',
        type: 'url',
        typeAttributes: {
            label: {fieldName: 'Name'},
            target: '_blank'
        }
    },
    {label: 'Type', fieldName: 'Type'},
    {label: 'Industry', fieldName: 'Industry'},
    {label: 'BillingStreet', fieldName: 'BillingStreet'},
    {label: 'Phone', fieldName: 'Phone', type: 'phone'},
    {label: 'AccountNumber', fieldName: 'AccountNumber'},
    {
        label: 'CreatedDate', fieldName: 'CreatedDate', type: 'date',
        typeAttributes: {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            hour12: true
        }
    },
    {label: 'Annual Revenue', fieldName: 'AnnualRevenue'},
    {
        label: 'Next Call-Back', fieldName: 'SLAExpirationDate__c', type: "nextCallbackColumn",
        typeAttributes: {
            nextCallbackDate: {fieldName: "SLAExpirationDate__c"}
        }
    }
];

export default class AccountDatatable extends NavigationMixin(LightningElement) {
    data = [];
    isNoAccountsFound = false;
    columns = columns;
    searchKey = '';
    timer;

    @track
    paginator = {
        pageNumber: {
            label: 1,
            value: 0
        },
        recordsPerPage: '10',
        totalPages: 1,
        totalRecords: 0,
        pageSizes: [
            { label: '3', value: '3' },
            { label: '10', value: '10' },
            { label: '50', value: '50' },
            { label: '100', value: '100' }
        ],

        next() {
            let value = this.pageNumber.value += 1;
            this.pageNumber = {label: value + 1, value: value};
        },

        previous() {
            let value = this.pageNumber.value -= 1;
            this.pageNumber = {label: value + 1, value: value};
        },

        setFirst() {
            this.pageNumber = {label: 1, value: 0};
        },

        setLast() {
            this.pageNumber = {label: this.totalPages, value: this.totalPages - 1};
        },

        setRecordsPerPage(value) {
            this.recordsPerPage = value;
        },

        calculateTotalPages() {
            this.totalPages = Math.ceil(this.totalRecords / this.recordsPerPage);
        }
    }


    async connectedCallback() {
        this.data = await getAccounts({searchKey: null, offset: 0, chunkSize: Number(this.paginator.recordsPerPage)});
        this.data = this._populateRecordUrls(this.data);
        this._checkIfAccountsFound(this.data);
        this.paginator.totalRecords = await getAccountsSize({searchKey: null});
        await this._calculateTotalPages();
    }

    handleRowAction(event) {
        const recordId = event.detail.row.Id;
        const actionName = event.detail.action.name;
        if (actionName === 'Edit') {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: recordId,
                    objectApiName: 'Account',
                    actionName: 'edit'
                }
            }).then((link) => {
                window.open(link, "_blank");
            });
        }
    }

    async handleSearch(event) {
        this.searchKey = event.target.value;
        this.paginator.setFirst();
        this.paginator.totalRecords = await getAccountsSize({searchKey: this.searchKey});
        this._debounce(debounceTimeout, () => {
            this._calculateTotalPages();
            this._retrieveAccounts();
        });
    }

    handlePageSizeChange(event) {
        this.paginator.setRecordsPerPage(event.target.value);
        this.paginator.setFirst();
        this._calculateTotalPages();
        this._retrieveAccounts();
    }

    _debounce(timeout, func) {
        if (this.timer !== null) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            func.apply();
            this.timer = null;
        }, timeout);
    }

    handleNavigationClick(event) {
        let name = event.target.name;
        if (name === "first") {
            this.handleFirst();
        } else if (name === "previous") {
            this.handlePrevious();
        } else if (name === "next") {
            this.handleNext();
        } else if (name === "last") {
            this.handleLast();
        }
        this._calculateTotalPages();
    }

    handleFirst() {
        if (this.paginator.pageNumber.value === 0) return;
        this.paginator.setFirst();
        this._retrieveAccounts();
    }

    handlePrevious() {
        if (this.paginator.pageNumber.value === 0) return;
        this.paginator.previous();
        this._retrieveAccounts();
    }

    handleNext() {
        if (this.paginator.pageNumber.value === this.paginator.totalPages - 1 || this.paginator.totalPages === 1) return;
        this.paginator.next()
        this._retrieveAccounts();
    }

    handleLast() {
        if (this.paginator.pageNumber.value === this.paginator.totalPages - 1 || this.paginator.totalPages === 1) return;
        this.paginator.setLast();
        this._retrieveAccounts();
    }

    _calculateTotalPages() {
        this.paginator.calculateTotalPages();
    }

    _retrieveAccounts() {
        let offset = Number(this.paginator.pageNumber.value * this.paginator.recordsPerPage);
        getAccounts({searchKey: this.searchKey, offset: offset, chunkSize: this.paginator.recordsPerPage}).then(data => {
            this._checkIfAccountsFound(data);
            this.data = this._populateRecordUrls(data);
        });
    }

    _populateRecordUrls(data) {
        return data.map(record => {
            return {...record, recordUrl: '/' + record.Id};
        })
    }

    _checkIfAccountsFound(data) {
        this.isNoAccountsFound = !data || !data.length;
    }

    get currentIsLast() {
        return this.paginator.pageNumber.value === this.paginator.totalPages - 1;
    }

    get currentIsFirst() {
        return this.paginator.pageNumber.value === 0;
    }
}