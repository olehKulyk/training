public class CompareCondition extends FieldCondition {

    public CompareCondition(String fieldName) {
        super(fieldName);
    }

    public CompareCondition(SObjectField fieldName) {
        super(fieldName);
    }

    public Condition equal(String fieldValue) {
        this.formedCondition = this.fieldName + ' = ' + this.quote(fieldValue);
        return this;
    }

    public Condition notEqual(String fieldValue) {
        this.formedCondition = this.fieldName + ' != ' + this.quote(fieldValue);
        return this;
    }

    public Condition equal(Boolean fieldValue) {
        this.formedCondition = this.fieldName + ' = ' + fieldValue;
        return this;
    }

    public Condition notEqual(Boolean fieldValue) {
        this.formedCondition = this.fieldName + ' != ' + fieldValue;
        return this;
    }

    public Condition equal(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' = ' + fieldValue;
        return this;
    }

    public Condition lessThan(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' < ' + fieldValue;
        return this;
    }

    public Condition greaterThan(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' > ' + fieldValue;
        return this;
    }

    public Condition lessThanOrEqual(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' <= ' + fieldValue;
        return this;
    }

    public Condition greaterThanOrEqual(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' >= ' + fieldValue;
        return this;
    }

    public Condition notEqual(Decimal fieldValue) {
        this.formedCondition = this.fieldName + ' != ' + fieldValue;
        return this;
    }
}