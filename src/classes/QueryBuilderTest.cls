@IsTest
public class QueryBuilderTest {

    @TestSetup
    public static void init() {
        Account account = createAccount('Test Account 1');
        createContact(account.Id);
    }

    @IsTest
    public static void testConstructor() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account').toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testAddFrom() {
        List<Account> accounts = (List<Account>) new QueryBuilder().addFrom('Account').toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testAddSubQuery() {
        List<Account> accounts = (List<Account>) new QueryBuilder().addFrom('Account')
                                                                   .addSubQuery(new QueryBuilder('Contacts')
                                                                           .addField('Name'))
                                                                   .toList();
        System.assertEquals(1, accounts.size());
        System.assertEquals(1, accounts[0]?.Contacts?.size());
    }

    @IsTest
    public static void testAddField() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account').addField('Name')
                                                                                      .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddListFields() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFields(new List<String>{'Id', 'Name'})
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddSetFields() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFields(new Set<String>{'Id', 'Name'})
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddAllFields() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFieldsAll()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddAllFieldsBySObject() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFieldsAll('Account')
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddAllCreatableFields() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFieldsAllCreatable()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testAddAllUpdatableFields() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addFieldsAllUpdatable()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
        System.assertNotEquals(null, accounts[0].Name);
    }

    @IsTest
    public static void testNullConditionByFieldNameIsNull() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new NullCondition('Name').isNull())
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testNullConditionBySObjectFieldIsNull() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new NullCondition(Account.Name).isNull())
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testNullConditionBySObjectFieldIsNotNull() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new NullCondition(Account.Name).notNull())
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testCompareConditionStringValueEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('Name').equal('Test Account 1'))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testCompareConditionStringValueEqualBySObjectField() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition(Account.Name).equal('Test Account 1'))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testCompareConditionStringValueNotEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('Name').notEqual('Test Account 1'))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionBooleanValueEqualBySObjectField() {
        List<Contact> contacts = (List<Contact>) new QueryBuilder('Contact')
                                                                 .addCondition(new CompareCondition(Contact.DoNotCall).equal(false))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, contacts.size());
    }

    @IsTest
    public static void testCompareConditionBooleanValueNotEqualByFieldName() {
        List<Contact> contacts = (List<Contact>) new QueryBuilder('Contact')
                                                                 .addCondition(new CompareCondition('DoNotCall').notEqual(false))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(contacts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').equal(1))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueNotEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').notEqual(0))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueGreaterThanByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').greaterThan(0))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueGreaterThanOrEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').greaterThanOrEqual(1))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueLessThanByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').lessThan(0))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testCompareConditionNumberValueLessThanOrEqualByFieldName() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').lessThanOrEqual(-1))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testLikeConditionAnyValues() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new LikeCondition('Name').likeAny('count'))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testLikeConditionAnyLeft() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new LikeCondition('Name').likeAnyLeft('est Account 1'))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testLikeConditionAnyRight() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new LikeCondition('Name').likeAnyRight('Test Acc'))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testInConditionBySetIds() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new InCondition('Id').inCollection(new Set<Id>()))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testInConditionByListIds() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new InCondition('Id').inCollection(new List<Id>()))
                                                                 .endConditions()
                                                                 .toList();
        System.assert(accounts.isEmpty());
    }

    @IsTest
    public static void testInConditionByList() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new InCondition('Name').inCollection(new List<String>{'Test Account 1'}))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testInConditionBySet() {
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new InCondition('Name').inCollection(new Set<String>{'Test Account 1'}))
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testConditionsWithOrder() {
        createAccount('Test Account 1', 100);
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addCondition(new CompareCondition('Name').equal('Test Account 1'))
                                                                 .addCondition(new CompareCondition('NumberOfEmployees').equal(0))
                                                                 .addConditionsWithOrder('1 AND 2')
                                                                 .endConditions()
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testAddLimit() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .setLimit(1)
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testAddOffset() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .setOffset(1)
                                                                 .toList();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testAddAscOrder() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('Name')
                                                                 .addOrderAsc('Name')
                                                                 .toList();
        System.assertEquals('Test Account 1', accounts[0].Name);
    }

    @IsTest
    public static void testSetSortingOrderAsc() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('Name')
                                                                 .addOrder(new Order('Name').setSortingOrderAsc())
                                                                 .toList();
        System.assertEquals('Test Account 1', accounts[0].Name);
    }

    @IsTest
    public static void testAddDescOrder() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('Name')
                                                                 .addOrderDesc('Name')
                                                                 .toList();
        System.assertEquals('Test Account 2', accounts[0].Name);
    }

    @IsTest
    public static void testSetSortingOrderDesc() {
        createAccount('Test Account 2');
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('Name')
                                                                 .addOrder(new Order(Account.Name).setSortingOrderDesc())
                                                                 .toList();
        System.assertEquals('Test Account 2', accounts[0].Name);
    }

    @IsTest
    public static void testSetNullsOrderFirst() {
        createAccount('Test Account 2', null);
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('NumberOfEmployees')
                                                                 .addOrder(new Order('NumberOfEmployees').setSortingOrderDesc()
                                                                                                                   .setNullsOrderFirst())
                                                                 .toList();
        System.assertEquals(null, accounts[0].NumberOfEmployees);
    }

    @IsTest
    public static void testSetNullsOrderLast() {
        createAccount('Test Account 2', null);
        List<Account> accounts = (List<Account>) new QueryBuilder('Account')
                                                                 .addField('NumberOfEmployees')
                                                                 .addOrder(new Order('NumberOfEmployees').setSortingOrderDesc()
                                                                                                                   .setNullsOrderLast())
                                                                 .toList();
        System.assertEquals(0, accounts[0].NumberOfEmployees);
    }

    @IsTest
    public static void testAddGroupBySingleField() {
        createAccount('Test Account 1', 25);
        List<AggregateResult> aggregateResults = (List<AggregateResult>) new QueryBuilder('Account')
                                                                                         .addField('Name')
                                                                                         .addGroupBy('Name')
                                                                                         .toList();
        System.assertEquals(1, aggregateResults.size());
    }

    @IsTest
    public static void testAddGroupByListFields() {
        createAccount('Test Account 1', 25);
        List<AggregateResult> aggregateResults = (List<AggregateResult>) new QueryBuilder('Account')
                                                                                         .addField('Name')
                                                                                         .addGroupBy(new List<String>{'Name', 'NumberOfEmployees'})
                                                                                         .toList();
        System.assertEquals(2, aggregateResults.size());
    }

    @IsTest
    public static void testAddGroupBySetFields() {
        createAccount('Test Account 1', 25);
        List<AggregateResult> aggregateResults = (List<AggregateResult>) new QueryBuilder('Account')
                                                                                         .addField('Name')
                                                                                         .addGroupBy(new Set<String>{'Name', 'NumberOfEmployees'})
                                                                                         .toList();
        System.assertEquals(2, aggregateResults.size());
    }

    @IsTest
    public static void testSetWithSecurityEnforced() {
        String query = new QueryBuilder('Account')
                                       .setWithSecurityEnforced()
                                       .toString();
        System.assert(query.contains('WITH SECURITY_ENFORCED'));
    }

    @IsTest
    public static void testGetConditionSize() {
        QueryBuilder queryBuilder = new QueryBuilder('Account')
                                                    .addField('Name')
                                                    .addCondition(new NullCondition('Name').notNull())
                                                    .addCondition(new CompareCondition('Name').equal('Test Account 1'))
                                                    .endConditions().preview();
        System.assertEquals(2, queryBuilder.getConditionSize());
    }

    @IsTest
    public static void testPreview() {
        new QueryBuilder('Account').preview();
    }

    @IsTest
    public static void testToString() {
        String queryString = new QueryBuilder('Account').toString();
        System.assertEquals('SELECT Id \nFROM Account', queryString);
    }

    @IsTest
    public static void testToCount() {
        Integer result = new QueryBuilder('Account').toCount();
        System.assertEquals(1, result);
    }

    @IsTest
    public static void testToCountStringWithConditions() {
        String queryString = new QueryBuilder('Account')
                                             .addCondition(new NullCondition('Name').notNull())
                                             .endConditions()
                                             .setLimit(2)
                                             .setOffset(1)
                                             .toStringCount();
        System.assertEquals('SELECT COUNT() FROM Account WHERE Name != NULL LIMIT 2 OFFSET 1', queryString);
    }

    @IsTest
    public static void testToMap() {
        Map<Id, SObject> accounts = new QueryBuilder('Account')
                                                    .addField('Name')
                                                    .toMap();
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testToMapWithArgs() {
        Map<Id, Account> accounts = new Map<Id, Account>();
        new QueryBuilder('Account').addField('Name').toMap(accounts);
        System.assertEquals(1, accounts.size());
    }

    @IsTest
    public static void testToIdSet() {
        Set<Id> accountIds = new QueryBuilder('Account').toIdSet();
        System.assertEquals(1, accountIds.size());
    }

    /*
       TODO refactoring
       Utility methods
    */
    private static Account createAccount(String accountName) {
        Account result = new Account();
        result.Name = accountName;
        result.NumberOfEmployees = 0;
        insert result;
        return result;
    }


    private static Account createAccount(String accountName, Integer numberOfEmployees) {
        Account result = new Account();
        result.Name = accountName;
        result.NumberOfEmployees = numberOfEmployees;
        insert result;
        return result;
    }
    private static Contact createContact(Id accId) {
        Contact result = new Contact();
        result.LastName = 'Contact-' + accId;
        result.AccountId = accId;
        result.Email = 'email' + accId + '@example.com';
        result.Description = accId;
        insert result;
        return result;
    }
}