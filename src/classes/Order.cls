public virtual class Order {

    private final String sortingField;
    private String sortingOrder;
    private String nullsOrder;

    protected Order(String sortingField, String sortingOrder, String nullsOrder) {
        this.sortingField = sortingField;
        this.sortingOrder = sortingOrder;
        this.nullsOrder = nullsOrder;
    }

    protected Order(String sortingField, String sortingOrder) {
        this(sortingField, sortingOrder, '');
    }

    public Order(String sortingField) {
        this(sortingField, 'ASC', '');
    }

    public Order(SObjectField sortingField) {
        this(sortingField.getDescribe().getName());
    }

    public Order setSortingOrder(String sortingOrder) {
        this.sortingOrder = sortingOrder;
        return this;
    }

    public Order setSortingOrderAsc() {
        return this.setSortingOrder('ASC');
    }

    public Order setSortingOrderDesc() {
        return this.setSortingOrder('DESC');
    }

    public Order setNullsOrder(String nullsOrder) {
        this.nullsOrder = nullsOrder;
        return this;
    }

    public Order setNullsOrderFirst() {
        return this.setNullsOrder('FIRST');
    }

    public Order setNullsOrderLast() {
        return this.setNullsOrder('LAST');
    }

    public override String toString() {
        return this.sortingField
                + ' ' + this.sortingOrder
                + (String.isNotEmpty(this.nullsOrder) ? ' NULLS ' + this.nullsOrder : '');
    }
}