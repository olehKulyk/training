public with sharing class ProjectDTOs {

    public class AccountDatatableDTO {
        @AuraEnabled public String fieldLabel;
        @AuraEnabled public String fieldName;
    }
}