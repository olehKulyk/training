public class LikeCondition extends FieldCondition {

    public LikeCondition(String fieldName) {
        super(fieldName);
    }

    public Condition likeAny(String fieldValue) {
        this.formedCondition = this.fieldName + ' LIKE ' + this.quote('%' + fieldValue + '%');
        return this;
    }

    public Condition likeAnyLeft(String fieldValue) {
        this.formedCondition = this.fieldName + ' LIKE ' + this.quote('%' + fieldValue);
        return this;
    }

    public Condition likeAnyRight(String fieldValue) {
        this.formedCondition = this.fieldName + ' LIKE ' + this.quote(fieldValue + '%');
        return this;
    }
}