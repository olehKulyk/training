public with sharing class AccountDatatableService {

    public static Integer getAccountsCount() {
        return new QueryBuilder('Account').toCount();
    }

    public static Integer getAccountsCount(String searchKey) {
        QueryBuilder queryBuilder = new QueryBuilder('Account');
        List<FieldSetMember> fieldSetMembers = getFieldsetFields();
        for (FieldSetMember fieldSetMember : fieldSetMembers) {
            SoapType fieldType = getFieldType(fieldSetMember);

            if (fieldType == SoapType.DATE || fieldType == SoapType.DATETIME || fieldType == SoapType.TIME) continue;

            if (fieldType == SoapType.STRING) {
                queryBuilder.addCondition(new LikeCondition(fieldSetMember.getFieldPath()).likeAny(searchKey));
                continue;
            }

            if (StringUtils.isNumeric(searchKey)) {
                queryBuilder.addCondition(new CompareCondition(fieldSetMember.getFieldPath()).equal(Decimal.valueOf(searchKey)));
            }
        }

        queryBuilder.addConditionsWithOrder(getConditionOrder(queryBuilder.getConditionSize()))
                    .endConditions();
        return queryBuilder.toCount();
    }
    
    public static List<Account> getAccounts(Integer offset, Integer chunkSize) {
        return new QueryBuilder('Account').addFieldSet('Base_Fields')
                                              .setLimit(chunkSize)
                                              .setOffset(offset)
                                              .toList();
    }

    public static List<Account> getAccounts(String searchKey, Integer offset, Integer chunkSize) {
        QueryBuilder queryBuilder = new QueryBuilder('Account').addFieldSet('Base_Fields');
        List<FieldSetMember> fieldSetMembers = getFieldsetFields();
        for (FieldSetMember fieldSetMember : fieldSetMembers) {
            SoapType fieldType = getFieldType(fieldSetMember);

            if (fieldType == SoapType.DATE || fieldType == SoapType.DATETIME || fieldType == SoapType.TIME) continue;

            if (fieldType == SoapType.STRING) {
                queryBuilder.addCondition(new LikeCondition(fieldSetMember.getFieldPath()).likeAny(searchKey));
                continue;
            }

            if (StringUtils.isNumeric(searchKey)) {
                queryBuilder.addCondition(new CompareCondition(fieldSetMember.getFieldPath()).equal(Decimal.valueOf(searchKey)));
            }
        }

        queryBuilder.addConditionsWithOrder(getConditionOrder(queryBuilder.getConditionSize()))
                    .endConditions()
                    .setLimit(chunkSize)
                    .setOffset(offset);
        return queryBuilder.toList();
    }

    private static String getConditionOrder(Integer size) {
        List<String> conditionPoints = new List<String>();
        for (Integer i = 1; i <= size; i++) {
            conditionPoints.add(String.valueOf(i));
        }
        return String.join(conditionPoints, ' OR ');
    }

    private static List<Schema.FieldSetMember> getFieldsetFields() {
        return SObjectType.Account.fieldSets.Base_Fields.getFields();
    }

    private static SoapType getFieldType(Schema.FieldSetMember fieldSetMember) {
        SObjectType sObjectType = Account.getSObjectType();
        DescribeSObjectResult describeSObjectResult = sObjectType.getDescribe();
        return describeSObjectResult.fields.getMap()
                                           .get(fieldSetMember.getFieldPath())
                                           .getDescribe()
                                           .getSoapType();
    }

    public static List<ProjectDTOs.AccountDatatableDTO> getAccountDatatableFields() {
        List<FieldSetMember> fieldSetMembers = getFieldsetFields();
        List<ProjectDTOs.AccountDatatableDTO> result = new List<ProjectDTOs.AccountDatatableDTO>();
        for (FieldSetMember fieldSetMember : fieldSetMembers) {
            ProjectDTOs.AccountDatatableDTO dto = new ProjectDTOs.AccountDatatableDTO();
            dto.fieldLabel = fieldSetMember.getLabel();
            dto.fieldName = fieldSetMember.getFieldPath();
            result.add(dto);
        }
        return result;
    }
}