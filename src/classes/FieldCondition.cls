public abstract class FieldCondition extends Condition {

    protected String fieldName;

    public FieldCondition(String fieldName) {
        super();
        this.fieldName = fieldName;
    }

    public FieldCondition(SObjectField fieldName) {
        super();
        this.fieldName = fieldName.getDescribe().getName();
    }

    public override Boolean isEmpty() {
        return String.isEmpty(this.fieldName);
    }
}