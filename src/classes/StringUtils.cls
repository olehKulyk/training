public with sharing class StringUtils {

    public static Boolean isNumeric(String s) {
        Boolean returnValue = true;
        try {
            Decimal.valueOf(s);
        } catch (Exception e) {
            returnValue = false;
        }
        return returnValue;
    }
}