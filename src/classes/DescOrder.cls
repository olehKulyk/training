public class DescOrder extends Order {

    public DescOrder(String sortingField) {
        super(sortingField, 'DESC');
    }
}