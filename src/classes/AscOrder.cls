public class AscOrder extends Order {

    public AscOrder(String sortingField) {
        super(sortingField, 'ASC');
    }
}