public with sharing class AccountDatatableController {

    @AuraEnabled
    public static List<Account> getAccounts(String searchKey, Integer offset, Integer chunkSize) {
        if (String.isBlank(searchKey)) {
            return AccountDatatableService.getAccounts(offset, chunkSize);
        } else {
            return AccountDatatableService.getAccounts(searchKey, offset, chunkSize);
        }
    }

    @AuraEnabled
    public static Integer getAccountsSize(String searchKey) {
        if (String.isBlank(searchKey)) {
            return AccountDatatableService.getAccountsCount();
        } else {
            return AccountDatatableService.getAccountsCount(searchKey);
        }
    }

    @AuraEnabled
    public static List<ProjectDTOs.AccountDatatableDTO> getAccountFields() {
        return AccountDatatableService.getAccountDatatableFields();
    }
}