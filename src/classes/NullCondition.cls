public class NullCondition extends FieldCondition {

    public NullCondition(String fieldName) {
        super(fieldName);
    }

    public NullCondition(SObjectField fieldName) {
        super(fieldName);
    }

    public Condition isNull() {
        this.formedCondition = this.fieldName + ' = NULL';
        return this;
    }

    public Condition notNull() {
        this.formedCondition = this.fieldName + ' != NULL';
        return this;
    }
}