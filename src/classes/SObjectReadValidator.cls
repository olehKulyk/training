public class SObjectReadValidator {
    private List<SObject> sObjectsToValidate;
    private SObjectType sObjectType;
    private DescribeSObjectResult describedSObjectType;

    public SObjectReadValidator(List<SObject> sObjectsToValidate) {
        this.sObjectsToValidate = sObjectsToValidate;
        this.sObjectType = sObjectsToValidate.getSObjectType();
        this.describedSObjectType = this.sObjectType.getDescribe();
    }

    public void validate() {
        this.checkObjectAccess();
    }

    public void checkObjectAccess() {
        if (!this.describedSObjectType.isAccessible() || !this.describedSObjectType.isQueryable()) {
            throw new System.NoAccessException();
        }
    }
}