public abstract class Condition {

    protected final String QUOTE = '\'';
    protected final String BRACKET_L = '(';
    protected final String BRACKET_R = ')';
    protected String formedCondition;

    public Condition() {
        this.formedCondition = '';
    }

    protected String quote(String field) {
        return this.QUOTE + String.escapeSingleQuotes(field.trim()) + this.QUOTE;
    }

    protected String bracket(String field) {
        return BRACKET_L + field.trim() + BRACKET_R;
    }

    public virtual override String toString() {
        return this.formedCondition;
    }

    public virtual Boolean isEmpty() {
        return String.isEmpty(this.toString());
    }
}