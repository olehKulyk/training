public class ConditionsManager {

    private final String BRACKET_L = '{';
    private final String BRACKET_R = '}';

    public List<Condition> conditions;
    private String conditionOrder;
    private final QueryBuilder queryBuilder;

    public String builtCondition;

    public ConditionsManager(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.conditions = new List<Condition>();
    }

    public ConditionsManager add(Condition condition) {
        if (condition.isEmpty()) {
            return this;
        }
        this.conditions.add(condition);
        return this;
    }

    public ConditionsManager setConditionOrder(String conditionOrder) {
        this.conditionOrder = conditionOrder;
        return this;
    }

    public QueryBuilder endConditions() {
        this.builtCondition = this.toString();
        return this.queryBuilder;
    }

    public override String toString() {
        String result = this.conditionOrder;
        if (String.isEmpty(result)) {
            result = '1';
        }
        result = this.bracketConditions(result);
        for (Integer i = 0; i < this.conditions.size(); i++) {
            Condition condition = this.conditions[i];
            String conditionNumber = this.bracket('' + (i + 1));
            if (result.contains(conditionNumber)) {
                result = result.replace(conditionNumber, condition.toString());
            }
        }
        return result;
    }

    public Boolean hasConditions() {
        return !this.conditions.isEmpty();
    }

    private String bracketConditions(String conditions) {
        this.conditions.size();
        for (Integer i = 1; i <= this.conditions.size(); i++) {
            String conditionNumber = '' + i;
            conditions = conditions.replace(conditionNumber, this.bracket(conditionNumber));
        }
        return conditions;
    }

    private String bracket(String condition) {
        return this.BRACKET_L + condition.trim() + this.BRACKET_R;
    }
}