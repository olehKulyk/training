public virtual with sharing class QueryBuilder {

    private String fromSobject;
    private Set<String> fieldTokens;
    private Set<String> groupByFields;
    private List<Order> sortingOrder;
    private Integer queryLimit;
    private Integer queryOffset;
    private Integer countResult;

    private ConditionsManager conditionsManager;

    private List<SObject> result;

    private Boolean withSecurityEnforced;
    private Boolean checkCrud;
    private Boolean checkFls;

    public QueryBuilder() {
        this.fieldTokens = new Set<String>();
        this.groupByFields = new Set<String>();
        this.sortingOrder = new List<Order>();
        this.queryLimit = 0;
        this.queryOffset = 0;
        this.countResult = null;
        this.conditionsManager = new ConditionsManager(this);
        this.withSecurityEnforced = false;
        this.checkCrud = false;
        this.checkFls = false;
    }

    public QueryBuilder(String fromSobject) {
        this();
        this.fromSobject = fromSobject.trim();
    }

    /* FROM */

    public QueryBuilder addFrom(String fromSobject) {
        this.fromSobject = fromSobject;
        return this;
    }

    /* ADD SUB-QUERY */

    public QueryBuilder addSubQuery(QueryBuilder queryBuilder) {
        return this.addSubQuery(queryBuilder.toString());
    }

    public QueryBuilder addSubQuery(String subQueryString) {
        this.fieldTokens.add('(' + subQueryString + ')');
        return this;
    }

    /* ADD FIELDS */

    public QueryBuilder addField(String fieldName) {
        this.fieldTokens.add(fieldName.trim());
        return this;
    }

    public QueryBuilder addFields(List<String> fieldNames) {
        for (String fieldName : fieldNames) {
            this.fieldTokens.add(fieldName.trim());
        }
        return this;
    }

    public QueryBuilder addFields(Set<String> fieldNames) {
        for (String fieldName : fieldNames) {
            this.fieldTokens.add(fieldName.trim());
        }
        return this;
    }

    public QueryBuilder addFieldsAll() {
        return this.addFieldsAll(this.fromSobject);
    }

    public QueryBuilder addFieldsAll(String sobjectName) {
        Set<String> fieldNames = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap().keySet();
        this.fieldTokens.addAll(fieldNames);
        return this;
    }

    public QueryBuilder addFieldsAllCreatable() {
        return this.addFieldsAllCreatable(this.fromSobject);
    }

    public QueryBuilder addFieldsAllCreatable(String sobjectName) {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(sobjectName);
        Map<String, Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();

        Set<String> createableFields = new Set<String>();
        for (Schema.SObjectField field : fieldMap.values()) {
            DescribeFieldResult fieldDescribe = field.getDescribe();
            if (fieldDescribe.isCreateable()) {
                createableFields.add(fieldDescribe.getName());
            }
        }
        fieldTokens.addAll(createableFields);
        return this;
    }

    public QueryBuilder addFieldsAllUpdatable() {
        return this.addFieldsAllUpdatable(this.fromSobject);
    }

    public QueryBuilder addFieldsAllUpdatable(String sobjectName) {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(sobjectName);
        Map<String, Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();

        Set<String> updateableFields = new Set<String>{
        };
        for (Schema.SObjectField field : fieldMap.values()) {
            DescribeFieldResult fieldDescribe = field.getDescribe();
            if (fieldDescribe.isUpdateable()) {
                updateableFields.add(fieldDescribe.getName());
            }
        }
        fieldTokens.addAll(updateableFields);
        return this;
    }

    /* ADD FIELDS FROM FIELD SET */

    public QueryBuilder addFieldSet(String fieldSetName) {
        return this.addFieldSet(this.fromSobject, fieldSetName);
    }

    public QueryBuilder addFieldSet(String objectName, String fieldSetName) {
        Set<String> result = new Set<String>();
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjectTypeObj = globalDescribeMap.get(objectName);
        List<FieldSetMember> fields = sObjectTypeObj.getDescribe().fieldSets.getMap().get(fieldSetName).getFields();
        for (FieldSetMember fieldSetMember : fields) {
            result.add(fieldSetMember.getFieldPath());
        }
        this.fieldTokens.addAll(result);
        return this;
    }

    public QueryBuilder addFieldSet(FieldSet fieldSet) {
        Set<String> fieldNames = new Set<String>();
        for (FieldSetMember fieldSetMember : fieldSet.getFields()) {
            fieldNames.add(fieldSetMember.getFieldPath());
        }
        this.fieldTokens.addAll(fieldNames);
        return this;
    }

    /* Conditions */

    public QueryBuilder addCondition(Condition condition) {
        this.conditionsManager.add(condition);
        return this;
    }

    public QueryBuilder endConditions() {
        return this.conditionsManager.endConditions();
    }

    public ConditionsManager addConditionsWithOrder(String conditionOrder) {
        return this.conditionsManager.setConditionOrder(conditionOrder);
    }

    /* LIMIT & OFFSET*/

    public QueryBuilder setLimit(Integer queryLimit) {
        this.queryLimit = queryLimit;
        return this;
    }

    public QueryBuilder setOffset(Integer queryOffset) {
        this.queryOffset = queryOffset;
        return this;
    }

    /* Order BY */

    public QueryBuilder addOrder(Order order) {
        this.sortingOrder.add(order);
        return this;
    }

    public QueryBuilder addOrderAsc(String fieldName) {
        this.sortingOrder.add(new AscOrder(fieldName));
        return this;
    }

    public QueryBuilder addOrderDesc(String fieldName) {
        this.sortingOrder.add(new DescOrder(fieldName));
        return this;
    }

    /* GROUP BY */

    public QueryBuilder addGroupBy(String field) {
        this.groupByFields.add(field);
        return this;
    }

    public QueryBuilder addGroupBy(List<String> fields) {
        this.groupByFields.addAll(fields);
        return this;
    }

    public QueryBuilder addGroupBy(Set<String> fields) {
        this.groupByFields.addAll(fields);
        return this;
    }

    /* SECURITY */

    public QueryBuilder setWithSecurityEnforced() {
        return this.setWithSecurityEnforced(true);
    }

    public QueryBuilder setWithSecurityEnforced(Boolean withSecurityEnforced) {
        this.withSecurityEnforced = withSecurityEnforced;
        return this;
    }

    public QueryBuilder setCheckCRUD(Boolean checkCrud) {
        this.checkCrud = checkCrud;
        return this;
    }

    public QueryBuilder setCheckCRUD() {
        return this.setCheckCRUD(true);
    }

    /* PREVIEW */

    public QueryBuilder preview() {
        String logRecord = '\n============RESULTING QUERY============\n'
                         + this.toString()
                         + '\n=======================================';
        System.debug(logRecord);
        return this;
    }

    public override String toString() {
        String result = 'SELECT ';
        String fields = '';
        if (this.fieldTokens.isEmpty()) {
            fields = 'Id';
        } else {
            fields = String.join(new List<String>(this.fieldTokens), '\n\t, ');
        }
        result += fields + ' \n';
        result += 'FROM ' + this.fromSobject + ' \n';
        if (this.conditionsManager.hasConditions()) {
            result += 'WHERE ' + this.conditionsManager.builtCondition + ' \n';
        }
        if (this.withSecurityEnforced) {
            result += 'WITH SECURITY_ENFORCED ';
        }
        if (!this.groupByFields.isEmpty()) {
            result += 'GROUP BY ';
            result += String.join(new List<String>(this.groupByFields), ', ') + ' \n';
        }
        if (!this.sortingOrder.isEmpty()) {
            result += 'ORDER BY ';
            List<String> orders = new List<String>();
            for (Order order : this.sortingOrder) {
                orders.add(order.toString());
            }
            result += String.join(orders, ', ') + ' \n';
        }
        if (this.queryLimit > 0) {
            result += 'LIMIT ' + this.queryLimit + ' \n';
        }
        if (this.queryOffset > 0) {
            result += 'OFFSET ' + this.queryOffset + ' \n';
        }
        return result.trim();
    }

    public Integer toCount() {
        if (this.countResult == null) {
            this.countResult = Database.countQuery(this.toStringCount());
        }
        return this.countResult;
    }

    public String toStringCount() {
        String result = 'SELECT ';
        String countString = 'COUNT() ';
        result += countString;
        result += 'FROM ' + this.fromSobject + ' ';
        if (this.conditionsManager.hasConditions()) {
            result += 'WHERE ' + this.conditionsManager.builtCondition + ' ';
        }
        if (this.queryLimit > 0) {
            result += 'LIMIT ' + this.queryLimit + ' ';
        }
        if (this.queryOffset > 0) {
            result += 'OFFSET ' + this.queryOffset + ' ';
        }
        return result.trim();
    }

    public Set<Id> toIdSet() {
        return this.toMap().keySet();
    }

    public Map<Id, SObject> toMap() {
        return new Map<Id, SObject>(this.toList());
    }

    public Map<Id, SObject> toMap(Map<Id, SObject> mapToFill) {
        for (SObject sObj : this.toList()) {
            Id sObjId = (Id) sObj.get('Id');
            mapToFill.put(sObjId, sObj);
        }
        return mapToFill;
    }

    public List<SObject> toList() {
        if (this.result == null) {
            List<SObject> result = Database.query(this.toString());
            if (this.checkCrud) {
                SObjectReadValidator sObjectReadValidator = new SObjectReadValidator(result);
                sObjectReadValidator.checkObjectAccess();
            }
            this.result = result;
        }
        return this.result;
    }

    public Integer getConditionSize() {
        return this.conditionsManager.conditions.size();
    }
}