public class InCondition extends FieldCondition {

    public InCondition(String fieldName) {
        super(fieldName);
    }

    public Condition inCollection(List<SObject> inList) {
        return this.inCollection(new Map<Id, SObject>(inList));
    }

    public Condition inCollection(Map<Id, SObject> inKeySet) {
        return this.inCollection(inKeySet.keySet());
    }

    public Condition inCollection(Set<String> inSet) {
        if (inSet.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildIn(inSet);
        return this;
    }

    public Condition inCollection(Set<Id> inSet) {
        if (inSet.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildIn(inSet);
        return this;
    }

    public Condition inCollection(Set<Decimal> inSet) {
        if (inSet.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildInNoQuote(inSet);
        return this;
    }

    private String buildIn(Set<String> inSet) {
        return this.bracket(
                this.QUOTE
                        + String.join(new List<String> (inSet), this.QUOTE + ', ' + this.QUOTE)
                        + this.QUOTE
        );
    }

    private String buildIn(Set<Id> inSet) {
        return this.bracket(
                this.QUOTE
                        + String.join(new List<Id> (inSet), this.QUOTE + ', ' + this.QUOTE)
                        + this.QUOTE
        );
    }

    private String buildInNoQuote(Set<Decimal> inSet) {
        return this.bracket(String.join(new List<Decimal> (inSet), ', '));
    }

    public Condition inCollection(List<Id> inList) {
        if (inList.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildIn(inList);
        return this;
    }

    public Condition inCollection(List<String> inList) {
        if (inList.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildIn(inList);
        return this;
    }

    public Condition inCollection(List<Decimal> inList) {
        if (inList.isEmpty()) {
            this.formedCondition = '';
        }
        this.formedCondition = this.fieldName + ' IN ' + this.buildInNoQuote(inList);
        return this;
    }

    public Condition inCollection(List<PicklistEntry> inList) {
        if (inList.isEmpty()) {
            this.formedCondition = '';
        }
        Set<String> picklistEntryValues = new Set<String>();
        for (PicklistEntry entry : inList) {
            picklistEntryValues.add(entry.value);
        }
        this.inCollection(picklistEntryValues);
        return this;
    }

    private String buildIn(List<String> inList) {
        return this.bracket(
                +this.QUOTE
                        + String.join(inList, this.QUOTE + ', ' + this.QUOTE)
                        + this.QUOTE
        );
    }

    private String buildIn(List<Id> inList) {
        return this.bracket(
                +this.QUOTE
                        + String.join(inList, this.QUOTE + ', ' + this.QUOTE)
                        + this.QUOTE
        );
    }

    private String buildInNoQuote(List<Decimal> inList) {
        return this.bracket(String.join(inList, ', '));
    }


    public Condition notIn(List<SObject> inList) {
        return this.notIn(new Map<Id, SObject>(inList));
    }

    public Condition notIn(Map<Id, SObject> inSet) {
        return this.notIn(inSet.keySet());
    }

    public Condition notIn(Set<Id> inSet) {
        if (inSet.isEmpty()) {
            this.formedCondition = '';
            return this;
        }
        this.formedCondition = this.fieldName + ' NOT IN ' + this.buildIn(inSet);
        return this;
    }

    public Condition notIn(QueryBuilder queryBuilder) {
        String builtSubQuery = queryBuilder.toString();
        if (String.isEmpty(builtSubQuery)) {
            this.formedCondition = '';
            return this;
        }
        this.formedCondition = this.fieldName + ' IN ' + this.bracket(builtSubQuery);
        return this;
    }
}